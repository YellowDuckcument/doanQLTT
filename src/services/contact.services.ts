import Contact from '~/models/schemas/contact.schemas';
// import Address from '~/models/schemas/address.schemas';
import databaseService from './database.services'
import { ObjectId } from 'mongodb';
import Address from '~/models/schemas/address.schemas';
import mongo_id from '~/helper';
import { pipeline } from 'stream';
// import { Address, Contact } from 'cluster';

class ContactService {
  async list(filter: object, options: {limit: number, skip: number}) {
    const result = await databaseService.contacts.find(filter, options);
    return result
  }

  async listV2(filter: object) {
    const pipeline = [
      {
        $match: filter
      },
      {
        $lookup: {
          from: 'connections',
          localField: '_id',
          foreignField: 'contact_id',
          as: 'connections'
        }
      }
      // SELECT * FROM connections, contacts WHERE connections.contact_id = contacts._id
    ]
    const result = await databaseService.contacts.aggregate(pipeline)
    return result
  }

  async count(filter: object) {
    return await databaseService.contacts.countDocuments(filter)
  }

  async createOne(
    payload: {
      firstName: string,
      lastName: string,
      email: string,
      phoneNumber: string,
      address: Address[]
    }
  ) {
    const {
      firstName,
      lastName,
      email,
      phoneNumber,
      address
    } = payload
    const result = await databaseService.contacts.insertOne(
      new Contact(firstName, lastName, email, phoneNumber, address)
    )
    return result
  }

  async updateOne(
    id: string,
    payload: {
      firstName: string,
      lastName: string,
      email: string,
      phoneNumber: string,
      address: Address[]
    }
  ) {
    const filter = {_id: mongo_id(id)}
    const updateDoc = {
      $set: payload
    }

    const result = await databaseService.contacts.findOneAndUpdate(
      filter,
      updateDoc
    )
    return result
  }

  async deleteOne(id: string) {
    const filter = {_id: mongo_id(id)}
    await databaseService.contacts.findOneAndDelete(filter)
  }

  async createMany(
    data: Contact[]
  ) {
    const result = await databaseService.contacts.insertMany(data)
    return result
  }
}

const contactService = new ContactService()
export default contactService
