import { config } from 'dotenv'
import { Collection, Db, MongoClient } from 'mongodb'
import User from '~/models/schemas/user.schemas'
import Contact from '~/models/schemas/contact.schemas'
import Address from '~/models/schemas/address.schemas'
import Country from '~/models/schemas/country.schemas'
import Province from '~/models/schemas/province.schemas'
import District from '~/models/schemas/district.schemas'
config()
const uri = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@facebook.taxv0n2.mongodb.net/?retryWrites=true&w=majority`
// const uri = `mongodb://localhost:27017`

class DatabaseService {
  private client: MongoClient
  private db: Db
  constructor() {
    this.client = new MongoClient(uri)
    this.db = this.client.db(process.env.DB_NAME)
  }
  async connect() {
    try {
      // Send a piMongoClientul connection
      await this.db.command({ ping: 1 })
      console.log('Pinged your deployment. You successfully connected to MongoDB!')
    } catch (err) {
      console.log('Error', err)
      throw err
    }
  }

  get users(): Collection<User> {
    return this.db.collection(process.env.DB_USER_COLLECTION as string)
  }

  get contacts(): Collection<Contact> {
    return this.db.collection(process.env.DB_CONTACT_COLLECTION as string)
  }

  get addresses(): Collection<Address> {
    return this.db.collection(process.env.DB_ADDRESS_COLLECTION as string)
  }

  get countries(): Collection<Country> {
    return this.db.collection(Country.collectionName as string)
  }

  get provinces(): Collection<Province> {
    return this.db.collection(Province.collectionName as string)
  }

  get districts(): Collection<District> {
    return this.db.collection(District.collectionName as string)
  }
}

// create a new object from DB service

const databaseService = new DatabaseService()
export default databaseService
