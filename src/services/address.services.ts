import Address from '~/models/schemas/address.schemas';
import databaseService from './database.services'
import { ObjectId } from 'mongodb';
import District from '~/models/schemas/district.schemas';
import Province from '~/models/schemas/province.schemas';

class AddressService {
  async listCountries() {
    const result = await databaseService.countries.find();
    return result
  }

  async listProvinces(id: string = '') {
    if (id) {
      return await databaseService.provinces.find({country_id: new ObjectId(id)})
    }
    return await databaseService.provinces.find()
  }

  async listDistricts(id: string = '') {
    if (id) {
      return await databaseService.districts.find({province_id: new ObjectId(id)})
    }
    return await databaseService.districts.find()
  }

  async randomProvince(filter: object = {}) {
    let result: Province[] = [];
    let provinces = await databaseService.provinces.find(filter);
    await provinces.forEach(doc => {
      result.push(doc)
    })
    let randomIndex = Math.floor(Math.random() * result.length);
    return result[randomIndex]
  }

  async randomDistrict(filter: object = {}): Promise<District> {
    let result: District[] = [];
    let districts = await databaseService.districts.find(filter);
    await districts.forEach(doc => {
      result.push(doc)
    })
    let randomIndex = Math.floor(Math.random() * result.length);
    return result[randomIndex]
  }
}

const addressService = new AddressService()
export default addressService
