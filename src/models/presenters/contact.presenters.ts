import addressService from "~/services/address.services"
import Address from "../schemas/address.schemas"

export default class ListContactPresenter{
    constructor(
        public id: string,
        public firstName: string,
        public lastName: string,
        public email: string,
        public address: Address[],
        public phoneNumber?: string,
    ) {
    }

    static transform(document: any): ListContactPresenter {
        let id = document._id.toString()

        return new ListContactPresenter(
            id,
            document.firstName,
            document.lastName,
            document.email,
            document.address,
            document.phoneNumber,
        )
    }
}