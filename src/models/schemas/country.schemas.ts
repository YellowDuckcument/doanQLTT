import { IntegerType, ObjectId } from "mongodb";

interface CountryType {
    iso: string,
    name: string,
    updated_at: Date,
    sort_index: IntegerType,
    alias: string
  }

export default class Country {
  static collectionName: string = "user_countries"
}