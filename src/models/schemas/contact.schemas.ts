
import { ObjectId } from 'mongodb'
import Address from './address.schemas';

// enum UserVerifyStatus {
//   Unverified, // chưa xác thực email, mặc định = 0
//   Verified, // đã xác thực email,
//   Banned // bị khóa
// }

export default class Contact {
  _id: ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber?: string;
  address?: Address[];

  constructor(
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber?: string,
    address?: Address[]
  ) {
    this._id = new ObjectId();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.address = address;
  }
}