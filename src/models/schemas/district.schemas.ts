import { IntegerType, ObjectId } from "mongodb";

interface DistrictType {
    _id: ObjectId,
    iso: string,
    name: string,
    updated_at: Date,
    sort_index: IntegerType,
    alias: string,
    country_id: ObjectId,
    province_id: ObjectId
  }

export default class District implements DistrictType {
  _id: ObjectId;
  iso: string;
  name: string;
  updated_at: Date;
  sort_index: IntegerType;
  alias: string;
  country_id: ObjectId;
  province_id: ObjectId;

  static collectionName: string = "user_towns"

  constructor(data: DistrictType) {
    this._id = data._id;
    this.iso = data.iso;
    this.name = data.name;
    this.updated_at = data.updated_at;
    this.sort_index = data.sort_index;
    this.alias = data.alias;
    this.country_id = data.country_id;
    this.province_id = data.province_id;
  }
}