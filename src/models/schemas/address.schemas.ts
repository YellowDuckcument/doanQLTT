import { ObjectId } from "mongodb";

interface AddressType {
    street: string;
    district: string;
    province: string;
    country: string;
  }

export default class Address {
  street: string;
  district: string;
  province: string;
  country: string;

  constructor(
    street: string,
    district: string,
    province: string,
    country: string,
  ) {
    this.street = street;
    this.district = district;
    this.province = province;
    this.country = country;
  }
}