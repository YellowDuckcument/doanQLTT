import express from 'express'
import 'reflect-metadata';
import path from 'path';
import userRouter from './routes/users.routes'
import contactRouter from './routes/contacts.routes'
import databaseService from './services/database.services'
import countryRouter from './routes/countries.routes'
import provinceRouter from './routes/provinces.routes';
import districtRouter from './routes/districts.routes';
import testRouter from './routes/test.routes';
const app = express()
const port = process.env.PORT

app.get('/', (req, res) => {
  res.render('index')
})

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json())
// app.use('/test', testRouter)
app.use('/countries', countryRouter)
app.use('/provinces', provinceRouter)
app.use('/districts', districtRouter)
app.use('/contacts', contactRouter)
app.use('/users', userRouter)
databaseService.connect()

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
