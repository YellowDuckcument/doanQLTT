import { ObjectId } from "mongodb";

export default function mongo_id(id: string): ObjectId
{
    return new ObjectId(id);
}