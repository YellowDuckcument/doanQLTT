import { Response } from 'express'

export const success = async (res: Response, result: any) => {
    return res.json({
        code: 0,
        msg: "success",
        data: result
    })
}

export const renderSuccess = async (res: Response, view: string, result: any) => {
    return res.render(
        view,
        {result}
    )
}
