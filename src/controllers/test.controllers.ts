import { Request, Response } from 'express'
import Contact from '~/models/schemas/contact.schemas'
import contactService from '~/services/contact.services'
import { success, renderSuccess } from './base.controllers'
import Address from '~/models/schemas/address.schemas'
import addressService from '~/services/address.services'
import ListContactPresenter from '~/models/presenters/contact.presenters'
import { da, fakerVI as faker } from '@faker-js/faker';
import databaseService from '~/services/database.services'
import { ObjectId } from 'mongodb'

export const test = async (req: Request, res: Response) => {
    console.log(faker.person.firstName())
    console.log(faker.person.lastName())

    const limit = 200;
    let start = 1;
    let data: Contact[] = []
    while (start <= limit) {
        const randomProvince = await addressService.randomProvince();
        const randomDistrict_1 = await addressService.randomDistrict({ province_id: new ObjectId(randomProvince._id) });
        const randomDistrict_2 = await addressService.randomDistrict({ province_id: new ObjectId(randomProvince._id) });

        const firstName = faker.person.firstName()
        const lastName = faker.person.lastName()
        const email = faker.internet.email()
        const phoneNumber = faker.phone.number()
        const address = [
            {
                street: faker.location.streetAddress(),
                district: randomDistrict_1.name,
                province: randomProvince.name,
                country: "Việt Nam"
            },
            {
                street: faker.location.streetAddress(),
                district: randomDistrict_2.name,
                province: randomProvince.name,
                country: "Việt Nam"
            }
        ]
        const newContact = new Contact(
            firstName,
            lastName,
            email,
            phoneNumber,
            address
        )
        data.push(newContact)
        console.log(start)
        start++
    }
    let result = contactService.createMany(data)
    success(res, result)
  }