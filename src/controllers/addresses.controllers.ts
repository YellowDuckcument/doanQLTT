import { Request, Response } from 'express'
import Contact from '~/models/schemas/contact.schemas'
import contactService from '~/services/contact.services'
import { success } from './base.controllers'
import Address from '~/models/schemas/address.schemas'
import addressService from '~/services/address.services'

export const listCountries = async (req: Request, res: Response) => {
  const result = await addressService.listCountries()

  let data: Object[] = []
  await result.forEach((document) => {
    data.push(document)
  })
  
  success(res, data)
}

export const listProvinces = async (req: Request, res: Response) => {
  let path = req.path
  let id = path.split('/').filter(s => s !== '')[0]

  let data: Object[] = []
  if (id) {
    const result = await addressService.listProvinces(id)
    await result.forEach((document) => {
      data.push(document)
    })
  }         
  
  success(res, data)
}

export const listDistricts = async (req: Request, res: Response) => {
  let path = req.path
  let id = path.split('/').filter(s => s !== '')[0]

  let data: Object[] = []
  if (id) {
    const result = await addressService.listDistricts(id)
    await result.forEach((document) => {
      data.push(document)
    })
  }         
  
  success(res, data)
}