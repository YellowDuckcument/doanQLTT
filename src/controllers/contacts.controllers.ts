import { Request, Response } from 'express'
import Contact from '~/models/schemas/contact.schemas'
import contactService from '~/services/contact.services'
import { success, renderSuccess } from './base.controllers'
import Address from '~/models/schemas/address.schemas'
import addressService from '~/services/address.services'
import ListContactPresenter from '~/models/presenters/contact.presenters'

export const list = async (req: Request, res: Response) => {
  const query = req.query
  let limit: number = parseInt((query.limit ?? "0").toString());
  let page: number = parseInt((query.page ?? "0").toString());
  let search: string = (query.search ?? "").toString()

  if (limit < 1) { limit = 0 }
  if (page < 1) { page = 0 }
  let skip = (page - 1) * limit;

  let filter = {}
  if (search && search !== "") {
    filter = {
      $or: [
        {lastName: {$regex: `.*${search}.*`, $options:"i"}},
        {firstName: {$regex: `.*${search}.*`, $options:"i"}},
        {email: {$regex: `.*${search}.*`, $options:"i"}},
        {phoneNumber: {$regex: `.*${search}.*`, $options:"i"}},
        {'address.country': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.street': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.province': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.district': {$regex: `.*${search}.*`, $options:"i"}},
      ]
    }
  }

  const options = {
    limit, skip
  }
  const result = await contactService.list(filter, options)
  const total = await contactService.count(filter)

  let items : ListContactPresenter[] = []
  await result.forEach((document) => {
    const presenter = ListContactPresenter.transform(document)
    items.push(presenter)
  })
  
  const meta = {
    total: total,
    count: items.length,
    per_page: limit,
    current_page: page,
    total_pages: Math.ceil(total/limit)
  }

  res.render('contact', {meta, items})
  // success(res, {meta, items})
}

export const listV2 = async (req: Request, res: Response) => {
  const query = req.query
  let limit: number = parseInt((query.limit ?? "0").toString());
  let page: number = parseInt((query.page ?? "0").toString());
  let search: string = (query.search ?? "").toString()

  if (limit < 1) { limit = 0 }
  if (page < 1) { page = 0 }
  let skip = (page - 1) * limit;

  let filter = {}
  if (search && search !== "") {
    filter = {
      $or: [
        {lastName: {$regex: `.*${search}.*`, $options:"i"}},
        {firstName: {$regex: `.*${search}.*`, $options:"i"}},
        {email: {$regex: `.*${search}.*`, $options:"i"}},
        {phoneNumber: {$regex: `.*${search}.*`, $options:"i"}},
        {'address.country': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.street': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.province': {$regex: `.*${search}.*`, $options:"i"}},
        {'address.district': {$regex: `.*${search}.*`, $options:"i"}},
      ]
    }
  }

  const options = {
    limit, skip
  }
  const result = await contactService.listV2(filter)
  const total = await contactService.count(filter)

  let items : object[] = []
  await result.forEach((document) => {
    items.push(document)
  })
  
  const meta = {
    total: total,
    count: items.length,
    per_page: limit,
    current_page: page,
    total_pages: Math.ceil(total/limit)
  }

  res.render('contact', {meta, items})
  // success(res, {meta, items})
}

export const createOne = async (req: Request, res: Response) => {
  const {
    firstName,
    lastName,
    email,
    phoneNumber,
    addresses
  } = req.body

  let address: Address[] = []
  if (addresses) {
    addresses.forEach((addr: {
      street: string,
      district: string,
      province: string,
      country: string
    }) => {
      let addrObj = new Address(addr.street, addr.district, addr.province, addr.country)
      address.push(addrObj)
    })
  }

  const contact = await contactService.createOne({
    firstName,
    lastName,
    email,
    phoneNumber,
    address
  })

  success(res, [])
}

export const updateOne = async (req: Request, res: Response) => {
  let params = req.params
  let id = params.id

  if (id) {
    const {
      firstName,
      lastName,
      email,
      phoneNumber,
      addresses
    } = req.body
    
    let address : Address[] = []
    if (addresses) {
      addresses.forEach((addr: {
        street: string,
        district: string,
        province: string,
        country: string
      }) => {
        let addrObj = new Address(addr.street, addr.district, addr.province, addr.country)
        address.push(addrObj)
      })
    }

    await contactService.updateOne(
      id,
      {
        firstName,
        lastName,
        email,
        phoneNumber,
        address
      }
    )
  }

  success(res, [])
}

export const deleteOne = async (req: Request, res: Response) => {
  
}