import express, { Router } from 'express'
import { loginController, registerController } from '~/controllers/users.controlers'
import loginValidator from '~/middlewares/user.middlewares'
const usersRouter = Router()

usersRouter.post('/login', loginValidator, loginController)
usersRouter.post('/register', registerController)

export default usersRouter
