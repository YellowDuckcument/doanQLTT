import express, { Router } from 'express'
import { listDistricts } from '~/controllers/addresses.controllers'
const districtRouter = Router()

districtRouter.get('/:id', listDistricts)

export default districtRouter
