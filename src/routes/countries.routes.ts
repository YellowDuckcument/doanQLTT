import express, { Router } from 'express'
import { listCountries } from '~/controllers/addresses.controllers'
const countryRouter = Router()

countryRouter.get('/', listCountries)

export default countryRouter
