import express, { Router } from 'express'
import { test } from '~/controllers/test.controllers'
import loginValidator from '~/middlewares/user.middlewares'
const testRouter = Router()

testRouter.post('/', test)

export default testRouter
