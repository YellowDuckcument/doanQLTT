import express, { Router } from 'express'
import { createOne, list, updateOne, deleteOne, listV2 } from '~/controllers/contacts.controllers'
const contactRouter = Router()

// usersRouter.post('/login', loginValidator, loginController)
// contactRouter.get('/', list)
contactRouter.get('/', listV2)
contactRouter.post('/', createOne)
contactRouter.patch('/:id', updateOne)
contactRouter.delete('/:id', deleteOne)

export default contactRouter
