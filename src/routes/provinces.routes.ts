import express, { Router } from 'express'
import { listProvinces } from '~/controllers/addresses.controllers'
const provinceRouter = Router()

provinceRouter.get('/:id', listProvinces)

export default provinceRouter
